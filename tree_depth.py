def solution(tree):
    if not tree:
        return 0
    depth = 0
    processed_root = False
    processed_left = False
    processed_right = False
    for element in tree:
        if not processed_root:
            depth += 1
            processed_root = True
            continue
        elif not processed_left:
            processed_left = True
            continue
        elif processed_left and not processed_right:
            processed_right = True
            continue
        elif processed_left and processed_right:
            depth += 1
            processed_right = False
    if processed_left or processed_right:
        depth += 1
    return depth

#print(solution(tree=[1, 2, 3, 4]))
print(solution(tree=[1, 1, -1, 1, -1]))
#print(solution(tree=[1, -1, 1, -1, -1, -1, 1]))
print(solution(tree=[1, 1, -1, 1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1]))
