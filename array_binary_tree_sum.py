def solution(arr):
    if not arr or len(arr) == 1:
        return ""
    nodes = []
    left_sum = 0
    right_sum = 0
    for index in range(len(arr)):
        head = arr[index]
        if head == -1:
            continue
        try:
            if not nodes:
                nleft = arr[index + 1]
            else:
                nleft = arr[len(nodes) + len(nodes) + 1]
        except IndexError:
            nleft = -1
        try:
            if not nodes:
                nright = arr[index + 2]
            else:
                nright = arr[len(nodes) + len(nodes) + 2]
        except IndexError:
            nright = -1
        if nleft == -1 and nright == -1:
            break
        nodes.append(node(head, nleft, nright))
    left_nodes = nodes[1::2]
    right_nodes = nodes[2::2]
    left_sum = nodes[0].left + sum([ n.get_sum() for n in left_nodes])
    right_sum = nodes[0].right + sum([ n.get_sum() for n in right_nodes])
    if left_sum < right_sum:
        return "Right"
    elif left_sum > right_sum:
        return "Left"
    return ""


class node:
    def __init__(self, head, left, right):
        self.head = head
        self.left = left if not left == -1 else 0
        self.right = right if not right == -1 else 0

    def __str__(self):
        return f"H:{self.head},L:{self.left},R:{self.right}"

    def __repr__(self):
        return self.__str__()

    def get_sum(self):
        return self.left + self.right

arr = [3,6,2,9,-1,10]
arr = [1, 4, 100, 5]
arr = [1, 10, 5, 1, 0, 6]
print(solution(arr))
