def solution(angles):
    output = []
    open_brackets = []
    closing_brackets = []
    for bracket in angles:
        if bracket == '<':
            open_brackets.append(bracket)
        else:
            try:
                open_brackets.pop()
            except:
                closing_brackets.append(bracket)
        output.append(bracket)
    output_str = ''.join(output)
    prepend = ""
    append = ""
    for extra_closed_brackets in closing_brackets:
        prepend += "<"
    for unclosed_brackets in open_brackets:
        append += ">"
    return f"{prepend}{output_str}{append}"

print(solution(angles="<<>>>>><<<>>"))
