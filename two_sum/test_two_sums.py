from unittest import TestCase
from two_sum import two_sums_brute, two_sums_pythonic
import random
import sys

class Test(TestCase):
    def __init__(self,*args, **kwargs):
        super(Test, self).__init__(*args,**kwargs)
        self.simple_test_1 = ([2, 7, 11, 15],9)
        self.simple_test_2 = ([3, 2, 4, 1, 9],12)

        self.no_solution_empty = ([],10)
        self.no_solution = ([1, 2, 3, 4],10)

        self.repeated_indicies = ([1, 2, 3, 4, 5],10)

        self.limit = 1000000
        self.random_test = (random.sample(range(0,sys.maxsize),self.limit),random.randint(0,sys.maxsize))

    def test_sums(self):
        nums, target = self.simple_test_1
        brute_indicies = two_sums_brute.solution(numbers=nums, target=target)
        pythonic_indicies = two_sums_pythonic.solution(numbers=nums, target=target)
        assert nums[brute_indicies[0]] + nums[brute_indicies[1]] == target
        assert nums[pythonic_indicies[0]] + nums[pythonic_indicies[1]] == target

        nums, target = self.simple_test_2

        brute_indicies = two_sums_brute.solution(numbers=nums, target=target)
        pythonic_indicies = two_sums_pythonic.solution(numbers=nums, target=target)
        assert nums[brute_indicies[0]] + nums[brute_indicies[1]] == target
        assert nums[pythonic_indicies[0]] + nums[pythonic_indicies[1]] == target

    def test_no_solution(self):
        nums, target = self.no_solution_empty

        brute_indicies = two_sums_brute.solution(numbers=nums, target=target)
        pythonic_indicies = two_sums_pythonic.solution(numbers=nums, target=target)
        assert brute_indicies == "No solution"
        assert pythonic_indicies == "No solution"

        nums, target = self.no_solution

        brute_indicies = two_sums_brute.solution(numbers=nums, target=target)
        pythonic_indicies = two_sums_pythonic.solution(numbers=nums, target=target)
        assert brute_indicies == "No solution"
        assert pythonic_indicies == "No solution"

    def test_repeated_brute_indicies(self):
        nums, target = self.repeated_indicies

        brute_indicies = two_sums_brute.solution(numbers=nums, target=target)
        pythonic_indicies = two_sums_pythonic.solution(numbers=nums, target=target)
        assert brute_indicies == "No solution"
        assert pythonic_indicies == "No solution"


    def test_random(self):
        nums, target = self.random_test

        # brute_indicies = two_sums_brute.solution(numbers=nums, target=target)
        brute_indicies = "No solution"
        pythonic_indicies = two_sums_pythonic.solution(numbers=nums, target=target)
        if isinstance(brute_indicies,str):
            assert brute_indicies == "No solution"
        else:
            assert nums[brute_indicies[0]] + nums[brute_indicies[1]] == target

        if isinstance(pythonic_indicies,str):
            assert pythonic_indicies == "No solution"
        else:
            assert nums[pythonic_indicies[0]] + nums[pythonic_indicies[1]] == target

