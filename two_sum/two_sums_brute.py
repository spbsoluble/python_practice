def solution(numbers: list ,target: int)-> list:
    for first_index in range(len(numbers)):
        filtered_numbers = numbers[first_index:]
        for second_index in range(len(filtered_numbers)):
            if first_index == first_index + second_index:
                # To prevent
                continue
            sum = numbers[first_index] + filtered_numbers[second_index]

            if sum == target:
                return [first_index,first_index+second_index]
    return "No solution"
