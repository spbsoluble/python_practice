def solution(numbers: list, target:int) -> list:
    sums_map = {}
    for i, num in enumerate(numbers):
        isum = target - num
        if isum not in sums_map:
            sums_map[num] = i
        else:
            return [sums_map[isum], i]
    return "No solution"
