
def solution(numbers):
    if not numbers or not isinstance(numbers,list):
        return 0
    return max(numbers)

numbers = [7,2,6,3]
print(solution(numbers))

